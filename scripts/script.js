'use strict'

const header = document.querySelector('header')
const main = document.querySelector('main')
const button = document.createElement('button')
const clients = document.querySelector('#clients')
button.classList.add('button', 'button-text')
button.innerText = 'Змінити тему'
header.appendChild(button)
button.addEventListener('click', changeTheme)

if (localStorage.getItem('theme') === 'alt') {
  header.classList.add('alt-theme')
  main.classList.add('alt-theme')
  clients.classList.add('alt-theme')
}

function changeTheme () {
  header.classList.toggle('alt-theme')
  main.classList.toggle('alt-theme')
  clients.classList.toggle('alt-theme')
  if (localStorage.getItem('theme') !== 'alt') {
    localStorage.setItem('theme', 'alt')
  } else {
    localStorage.removeItem('theme')
  }
}
